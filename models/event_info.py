from odoo import api, fields, models

class Event_information(models.Model):
	
	_name = 'event.info'

	_rec_name = 'name'

	name = fields.Char('Event Name')
	start_date = fields.Date('Start Date')
	end_date = fields.Date('End Date')

	
Event_information()