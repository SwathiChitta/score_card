from odoo import api, fields, models

class Person_profile(models.Model):

	_name = 'person.profile'

	_rec_name = 'name'

	logo = fields.Binary('Profile Photo')
	name = fields.Char('Name')
	person_id = fields.Char('ID')
	department = fields.Many2one('department.info','Department')

Person_profile()


class Departments_information(models.Model):

	_name = 'department.info'

	_rec_name = 'name'

	name = fields.Char('Department Name', required=True)

Departments_information()