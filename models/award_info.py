from odoo import api, fields, models


class Award_information(models.Model):

	_name = 'award.info'

	_rec_name = 'name'

	name = fields.Char('Name', required=True)
	category_id = fields.Many2one('sub.category','Category')


Award_information()


class Sub_category(models.Model):

	_name = 'sub.category'

	_rec_name = 'name'

	name = fields.Char('Name', required=True)
	parent = fields.Many2one('parent.category','Parent Category')
	
	_sql_constraints = [('unique_model', 'unique(parent, name)', 'Cannot Use same name for this Sub Category!')]


Sub_category()



class Parent_categorie(models.Model):
	
	_name = 'parent.category'

	_rec_name = 'name'

	name = fields.Char('Name', required=True)

	_sql_constraints = [
		('name_uniq', 'UNIQUE (name)',  'You can not have two categories with the same name !')
	]

Parent_categorie()
