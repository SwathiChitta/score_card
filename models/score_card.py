from odoo import api, fields, models

class Score_cards_information(models.Model):
	_name = 'score.cards'

	_rec_name = 'name'

	name = fields.Char('Score Card Name', required=True)
	event = fields.Many2one('event.info','Event')
	person = fields.Many2one('person.profile','Person')
	start_date = fields.Date('Start Date')
	end_date = fields.Date('End Date')
	marks = fields.Float('Marks')
	total_marks = fields.Float('Total Marks')
	sub_category = fields.Many2one('sub.category','Sub Category')
	percentage = fields.Float('Percentage')


	@api.onchange('start_date')
	def _onchange_start_date(self):
		if self.start_date:
			events = self.env['event.info'].search([('start_date','=',self.start_date)]).ids
			return {'domain': {'event': [('id','in',events)]}}
			
Score_cards_information()