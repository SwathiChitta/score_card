{
	'name': 'Score Card',
	'version': '1.0',
	'category': 'Score Cards',

	'description': """
	Score Card Information Module
	""",

	'author': 'ehAPI Team',
	'website': 'www.ehapi.com',
	'depends': ['base'],

	'data': [
		'security/score_card_security_view.xml',
		'security/ir.model.access.csv',

		'reports/score_card_report.xml',

		'views/main_menus_view.xml',
		'views/award_info_view.xml',
		'views/event_info_view.xml',
		'views/person_profile_view.xml',
		'views/score_card_view.xml',
		'views/configuration_view.xml',
		],

	'qweb': [ ],
	
	'installable': True,
	'auto_install': False,
	'application': True,
}
